package com.example.demo.controler;

import com.example.demo.model.Movie;
import com.example.demo.repository.MovieRepository;
import com.example.demo.service.MovieService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

/*
@RestController
public class BasicControler {
    @RequestMapping("/")
    String hola(){
        return "HolaMundo";
    }
    @RequestMapping("/hola2")
    String hola2(){
        return "HolaMundo2";
    }

}
 */
@Controller
public class BasicControler {
    //esto para poder meter pelis
    @Autowired
    private MovieService movieService;

    @RequestMapping("/")
    String hola() {
        //devuelve la vista index
        return "index";
    }
    @RequestMapping("/add")
    String add(){
        //devuelve la vista addMovie
        return "addMovie";
    }
    @RequestMapping("/delete")
    String delete(){
        //devuelve la vista addMovie
        return "deleteMovie";
    }
    @RequestMapping("/update")
    String update(){
        //devuelve la vista addMovie
        return "updateMovie";
    }
    @PostMapping("/add")
    String prueba(@RequestParam String movie_name, @RequestParam String movie_url, Model model){
        Movie m = new Movie();
        m.setUrl(movie_url);
        m.setMovie_name(movie_name);
        movieService.addMovie(m);

        model.addAttribute("message", "La película '" + movie_name + "' ha sido añadida.");
        return "index";
    }
    @DeleteMapping("/delete")
    String borrar(@RequestParam String movie_id, Model model){

        Long id = Long.parseLong(movie_id);
        /*
        movieService.deleteMovie(holaPaloma);
        model.addAttribute("message", "La película '" + movie_id + "' ha sido borrada.");

         */
        Movie mexiste = movieService.getMovieById(id);
        if(mexiste != null){
            movieService.deleteMovie(id);
            model.addAttribute("message", "La película '" + movie_id + "' ha sido borrada.");
        }else{
            model.addAttribute("message", "Ese ID no esxiste uwu");
        }
        return "index";
    }
    @PutMapping("/update")
    String actualizar(@RequestParam String movie_id, @RequestParam String movie_name,@RequestParam String movie_url,Model model){

        Long id = Long.parseLong(movie_id);

        Movie mexiste = movieService.getMovieById(id);
        if(mexiste != null){
            Movie m = new Movie();
            if(movie_name == null || movie_name.trim().equals("")){
                m.setMovie_name(mexiste.getMovie_name());
            }else{
                m.setMovie_name(movie_name);
            }

            if(movie_url == null || movie_url.trim().equals("")){
                m.setUrl(mexiste.getUrl());
            }else{
                m.setUrl(movie_url);
            }
            movieService.updateMovie(id,m);
            model.addAttribute("message", "La película '" + movie_id + "' ha sido actualizada con el nombre: " + movie_id + " y la url: " + movie_url);
        }else{
            model.addAttribute("message", "Ese ID no esxiste pelotudo");
        }
        return "index";
    }
}

/*
* Un controlador va controlar mapas.
* vamos a partir de localhosto8080, es decir, la url base
*
* */
