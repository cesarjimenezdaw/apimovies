package com.example.demo.controler;

import com.example.demo.model.Movie;
import com.example.demo.service.MovieService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/movies")
public class MovieControler {
    @Autowired
    private MovieService movieService;

    @GetMapping
    public List<Movie> getMovies(){
        return movieService.getAllMovies();
    }
    @GetMapping("/{id}")
    public Movie getMovie(@PathVariable Long id) {
        return movieService.getMovieById(id);
    }
    @PostMapping
    public Movie addMovie(@RequestBody Movie movie) {
        //esto creo que al final no lo usamos para nada porque no le metemos una movie en el request
        Movie m = new Movie();
        m.setMovie_name("");
        m.setUrl("");
        return movieService.addMovie(movie);
    }
    @PutMapping("/{id}")
    public Movie updateMovie(@PathVariable Long id, @RequestBody Movie movie) {
        return movieService.updateMovie(id, movie);
    }
    @DeleteMapping("/{id}") public void deleteMovie(@PathVariable Long id) {
        movieService.deleteMovie(id);
    }

}
